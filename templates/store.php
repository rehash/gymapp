<?php GA::debug($products); ?>
<div class="wrap">
	<h2>Store</h2>

	<div class="row">
		<div class="products">
			<?php
			if ( $products->have_posts() )
			{
				while ( $products->have_posts() )
				{
					$products->the_post();
					?>
					<div class="product">
						<?php echo get_the_title(); ?>
					</div>
					<?php
				}
			}
			?>
		</div>

		<div class="totals">
			
		</div>
	</div>
</div>