<div class="ga-traffic-members">
	<p><?php echo __('Type the name or barcode:', 'gymapp'); ?></p>
	<form name="checkin" id="ga-checkin">
		<input type="text" name="ga-cqs" id="ga-dashboard-checkin-qs">
		<button type="submit" class="button button-primary" id="ga-dashboard-checkin-search">Search</button>
	</form>

	<div id="ga-checkin-search-results"></div>
</div>