<div class="ga-traffic-members">
	<p><?php echo __('These members are in the gym right now:', 'gymapp'); ?></p>
	<?php

	if ( $nowin && is_array( $nowin ) )
	{
		foreach ( $nowin as $member )
		{
		?>
		<div class="ga-traffic-member">
			
			<?php echo wp_get_attachment_image( get_field('headshot', $member->member_id), array(32,32) ); ?>
		</div>
		<?php
		} //--> endforeach
	} //--> endif
	?>
</div>