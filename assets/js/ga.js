(function ($) {
	$(document).ready(function () {
		// $('#ga-dashboard-checkin-search').on( 'click', function (e) {
		$(document).on('submit', 'form#ga-checkin', function (e) {
			e.preventDefault();
			var data = {
				action: 'ga-ajax',
				ga_action: 'checkin',
				queryString: $('#ga-dashboard-checkin-qs').val()
			};
			$.ajax({
				type: 'POST',
	        	url: ajaxurl,
	        	data: data,
	        	success: function (res) {
	        		$('#ga-checkin-search-results').html(res);
	        	}
			});
		}); // checkin search button click event
	}); // document ready
})(window.jQuery);