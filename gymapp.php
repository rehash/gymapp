<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.example.com
 * @since             1.0.0
 * @package           GYMapp GYM Management
 *
 * @wordpress-plugin
 * Plugin Name:       GYMapp GYM Management
 * Plugin URI:        https://www.example.com
 * Description:       GYMapp GYM Management is a main plugin full of hooks and filters so it can be extended with extension plugins.
 * Version:           1.0.0
 * Author:            Iuhas I. Daniel
 * Author URI:        https://www.example.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       exp
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

require plugin_dir_path( __FILE__ ) . 'includes/class.gymapp.php';

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-webcal-activator.php
 */
function activate_gymapp() {
	require plugin_dir_path( __FILE__ ) . 'includes/class.activator.php';
	$GA_Activator = new GA_Activator;
	$GA_Activator->activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-webcal-deactivator.php
 */
function deactivate_gymapp() {
	require plugin_dir_path( __FILE__ ) . 'includes/class.deactivator.php';
	$GA_Deactivator = new GA_Deactivator;
	$GA_Deactivator->deactivate();
}

register_activation_hook( __FILE__, 'activate_gymapp' );
register_deactivation_hook( __FILE__, 'deactivate_gymapp' );


$ga = new GA;
$ga->init();