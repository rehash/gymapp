<?php

class GA
{
	public $plugin_slug 	= 'ga';
	public static $userInfo = null;

	function init()
	{
		self::$userInfo = array(
			'user_id' 	=> get_current_user_id(),
			'gym_id' 	=> $this->ga_get_current_user_gym_id()
		);

		add_action( 'wp_loaded', 							array( $this, 'ga_flush_rules' ) );
		add_action( 'admin_menu', 							array( $this, 'ga_create_admin_menu' ), 9 );
		add_action( 'init', 								array( $this, 'ga_register_cpts' ) );

		add_filter( 'template_redirect', 					array( $this, 'ga_page_template' ) );

		add_action(	'wp_dashboard_setup', 					array( $this, 'ga_remove_dashboard_widgets' ) );


		add_action( 'wp_dashboard_setup', 					array( $this, 'ga_users_today_dashboard_widget' ) );

		add_action( 'admin_init',  							array( $this, 'ga_enqueue_scripts' ) );


		// ################################################################################ //
		// # -------------------------- COPY FROM HERE ---------------------------------- # //
		// ################################################################################ //

		add_action( 'user_register', 						array( $this, 'ga_registration_save' ), 10, 1 );

		// add the field to user's own profile editing screen
		add_action(
		    'user_new_form',
		    array( $this, 'ga_usermeta_form_field_gym_id' )
		);

		// add the field to user's own profile editing screen
		add_action(
		    'edit_user_profile',
		    array( $this, 'ga_usermeta_form_field_gym_id' )
		);
		 
		// add the field to user profile editing screen
		add_action(
		    'show_user_profile',
		    array( $this, 'ga_usermeta_form_field_gym_id' )
		);
		 
		// add the save action to user's own profile editing screen update
		add_action(
		    'personal_options_update',
		    array( $this, 'ga_usermeta_form_field_gym_id_update' )
		);
		 
		// add the save action to user profile editing screen update
		add_action(
		    'edit_user_profile_update',
		    array( $this, 'ga_usermeta_form_field_gym_id_update' )
		);


		// do_action( 'pre_get_users', WP_User_Query $this )
		add_filter( 'pre_get_users', array( $this, 'ga_adm_list_users' ) );





		// Register CYMappCC Class for Custom Post Type Custom Columns
		require plugin_dir_path( __FILE__ ) . 'custom/class.ga-cpt.php';
		$gaCpt 	= new GA_CPT;
		$gaCpt->init();

		require plugin_dir_path( __FILE__ ) . '/class.ga-save.php';
		$gaSave = new GA_Save;
		$gaSave->init();

		// Traffic specific functions
		require plugin_dir_path( __FILE__ ) . '/class.ga-traffic.php';
		// Search specific functions
		require plugin_dir_path( __FILE__ ) . 'custom/class.ga-search.php';


		// AJAX
		add_action( 'wp_ajax_' . $this->plugin_slug . '-ajax', array( $this, 'ga_ajax' ) );

		// add_action('admin_menu', array( $this, 'remove_built_in_roles' ) );
	}

	function ga_adm_list_users( $query )
	{
		global $pagenow;

		if ( current_user_can('list_users') )
		{
	        $meta_query = array(
	            array(
	                'key' => 'user_created',
	                'value' => get_current_user_id()
	            )
	        );
	        $query->set( 'meta_key', 'user_created' );
	        $query->set( 'meta_query', $meta_query );
		}
	}

	function ga_registration_save( $user_id )
	{
		add_user_meta( $user_id, 'user_created', get_current_user_id() );
	}

	/**
	 * The field on the editing screens.
	 *
	 * @param $user WP_User user object
	 */
	function ga_usermeta_form_field_gym_id($user)
	{
	    ?>
	    <h3>Gym to append</h3>
	    <table class="form-table">
	        <tr>
	            <th>
	                <label for="gym_id">Gym to append</label>
	            </th>
	            <td>
					<select name="gym_id">
					<?php
	                $args = array(
	                	'post_type' => 'gyms',
	               	);

	               	$gyms = new WP_Query( $args );

	               	if ( $gyms->have_posts() )
	               	{
	               		foreach ( $gyms->posts as $gym )
	               		{
	               			?><option <?php if (get_user_meta($user->data->ID,'gym_id',true) == $gym->ID): ?>selected="selected"<?php endif; ?> value="<?php echo $gym->ID; ?>"><?php echo $gym->post_title; ?></option><?php
	               		}
	               	}
	                ?>
	            	</select>
	                <p class="description">
	                    Please select your GYM.
	                </p>
	            </td>
	        </tr>
	    </table>
	    <?php
	}
	 
	/**
	 * The save action.
	 *
	 * @param $user_id int the ID of the current user.
	 *
	 * @return bool Meta ID if the key didn't exist, true on successful update, false on failure.
	 */
	function ga_usermeta_form_field_gym_id_update($user_id)
	{
	    // check that the current user have the capability to edit the $user_id
	    if (!current_user_can('edit_user', $user_id)) {
	        return false;
	    }
	 
	    // create/update user meta for the $user_id
	    return update_user_meta(
	        $user_id,
	        'gym_id',
	        $_POST['gym_id']
	    );
	}

	function ga_get_current_user_gym_id()
	{
		return get_user_meta( self::$userInfo['user_id'], 'gym_id', true );
	}


	function ga_enqueue_scripts()
	{
		wp_enqueue_style( 'ga-style', plugin_dir_url( __FILE__ )  . '../assets/css/ga.css');

		wp_register_script( 'ga-script', plugin_dir_url( __FILE__ ) . '../assets/js/ga.js', array( 'jquery' ) );
		wp_localize_script( 'ga-script', 'ga_ajaxurl', admin_url( 'admin.php?page=ga-ajax&do=checkin&noheader=true' ) );

		wp_enqueue_script( 'ga-script' );
	}



	function ga_users_today_dashboard_widget()
	{
		wp_add_dashboard_widget(
                 'users_today',         // Widget slug.
                 'Users Today',         // Title.
                 array( $this, 'ga_users_today_dashboard_widget_function' ) // Display function.
        );

        wp_add_dashboard_widget(
                 'checkin',         // Widget slug.
                 'Checkin',         // Title.
                 array( $this, 'ga_checkin_dashboard_widget_function' ) // Display function.
        );

        wp_add_dashboard_widget(
                 'traffic',         // Widget slug.
                 'Traffic',         // Title.
                 array( $this, 'ga_traffic_dashboard_widget_function' ) // Display function.
        );
	}

	function ga_checkin_dashboard_widget_function() 
	{
		require_once plugin_dir_path( __FILE__ ) . '../templates/widgets/checkin.php';
	}

	function ga_traffic_dashboard_widget_function()
	{
		$gaTraffic = new GA_Traffic;
		// get members currently in the gym
		$nowin 	= $gaTraffic->ga_get_traffic( 21 );

		require_once plugin_dir_path( __FILE__ ) . '../templates/widgets/traffic.php';
	}

	function ga_users_today_dashboard_widget_function()
	{
		$today = getdate();
		$args = array(
		    'date_query' => array(
		        array(
		            'year'  => $today['year'],
		            'month' => $today['mon'],
		            'day'   => $today['mday'],
		        ),
		    ),
		    'post_type' => 'users'
		);
		$custom_query = new WP_Query( $args );

		// Todays total members
		$total_members 		= $custom_query->post_count;
		// Todays total amount
		$total_amount 		= 0;

		$members 			= $custom_query->posts;
		
		if ( $custom_query->have_posts() )
		{
			while ( $custom_query->have_posts() )
			{
				$custom_query->the_post();

				$membership 	= get_field('membership');
				$price 			= get_post_meta($membership[0], 'price');
				$total_amount 	= $total_amount + $price[0];

			}
			wp_reset_postdata();
		}

		$gaTraffic = new GA_Traffic;

		$traffic_today = $gaTraffic->ga_get_traffic_today( 21 );

		require_once plugin_dir_path( __FILE__ ) . '../templates/widgets/todays-totals.php';
	}

	// flush_rules() if our rules are not yet included
	function ga_flush_rules(){
		// global $wp_rewrite;
	 //   	$wp_rewrite->flush_rules();
		// $rules = get_option( 'rewrite_rules' );

		// if ( ! isset( $rules['(user)/(\d*)$'] ) ) {
		// }
	}

	function ga_create_admin_menu()
	{
		add_menu_page( __('Settings'), __('Settings'), 'read', $this->plugin_slug . '-settings', array( $this, 'ga_settings' ), 'dashicons-admin-settings', 2 );
		add_menu_page( __('Store'), __('Store'), 'read', $this->plugin_slug . '-store', array( $this, 'ga_store'), 'dashicons-cart', 3 );
	}

	function ga_store()
	{
		require plugin_dir_path( __FILE__ ) . '/class.ga-store.php';
		$gaStore = new GA_Store;
		$gaStore->init();

		$products = $gaStore->get_products();

		require_once plugin_dir_path( __FILE__ ) . '../templates/store.php';
	}

	function ga_dashboard()
	{

	}

	function ga_settings()
	{

	}

	// Handling AJAX requests
	function ga_ajax()
	{
		// global $wpdb;
		$data = $_POST;

		switch ( $data['ga_action'] )
		{
			case 'checkin':
				if ( !empty($data['queryString']) )
				{
					$search_query 	= sanitize_text_field( $data['queryString'] );

					$args 		= array(
						'post_type' 			=> 'users',
						'post_fields_to_search' => array('post_title'),
						'meta_fields_to_search' => array('barcode'),
						'search_term' 			=> $search_query
					);
					$gaSearch 	= new GA_Search;
					$results 	= $gaSearch->ga_search_cpt_by_post_and_meta_fields($args);

					foreach ( $results as $user )
					{
						?>
						<div class="ga-member">
							<?php echo wp_get_attachment_image( get_field('headshot', $user), array(32,32) ); ?>
							<a href="#"><?php echo get_the_title($user); ?></a>
						</div>
						<?php
					}
				}
				break;
		}

		exit;

	}

	function ga_register_cpts()
	{
		register_post_type(
			'gyms',
			array(
				'label' 					=> 'Gyms',
				'public' 					=> false,
				'exclude_from_search'		=> false,
				'show_ui' 					=> true,
				'show_in_menu'				=> true,
				'menu_position'				=> 4,
				'menu_icon' 				=> 'dashicons-admin-multisite',
				'rewrite'					=> array(
							'slug' 			=> 'gym'
				),
				'capabilities'				=> array(
					'edit_post'          => 'edit_gym',
					'read_post'          => 'read_gym',
					'delete_post'        => 'delete_gym',
					'edit_posts'         => 'edit_gyms',
					'edit_others_posts'  => 'edit_others_gyms',
					'publish_posts'      => 'publish_gyms',  
					'read_private_posts' => 'read_private_gyms',
					'create_posts'       => 'edit_gyms', 
				)
			)	
		);

		register_post_type(
			'class',
			array(
				'label' 					=> 'Classes',
				'public' 					=> false,
				'exclude_from_search'		=> false,
				'publicly_queryable'		=> true,
				'show_ui' 					=> true,
				// 'show_in_menu'				=> $this->plugin_slug,
				'show_in_menu'				=> true,
				'menu_position'				=> 4,
				'supports' 					=> array('title'),
				'menu_icon'					=> 'dashicons-calendar',
				'rewrite'					=> array(
							'slug' 			=> 'class'
				),
				'capabilities'				=> array(
					'edit_post'          => 'edit_class',
					'read_post'          => 'read_class',
					'delete_post'        => 'delete_class',
					'edit_posts'         => 'edit_classes',
					'edit_others_posts'  => 'edit_others_classes',
					'publish_posts'      => 'publish_classes',  
					'read_private_posts' => 'read_private_classes',
					'create_posts'       => 'edit_classes', 
				)
			)	
		);


		register_post_type(
			'users',
			array(
				'label' 					=> 'Members',
				'public' 					=> false,
				'exclude_from_search'		=> false,
				'publicly_queryable'		=> true,
				'show_ui' 					=> true,
				// 'show_in_menu'				=> $this->plugin_slug,
				'show_in_menu'				=> true,
				'menu_position'				=> 4,
				'supports' 					=> array('title'),
				'menu_icon'					=> 'dashicons-groups',
				'rewrite'					=> array(
							'slug' 			=> 'user'
				),
				'capabilities'				=> array(
					'edit_post'          => 'edit_user',
					'read_post'          => 'read_user',
					'delete_post'        => 'delete_user',
					'edit_posts'         => 'edit_users',
					'edit_others_posts'  => 'edit_others_users',
					'publish_posts'      => 'publish_users',  
					'read_private_posts' => 'read_private_users',
					'create_posts'       => 'edit_users', 
				)
			)	
		);

		register_post_type(
			'products',
			array(
				'label' 					=> 'Products',
				'public' 					=> false,
				'exclude_from_search'		=> false,
				'show_ui' 					=> true,
				'show_in_menu'				=> $this->plugin_slug . '-store',
				// 'show_in_menu'				=> true,
				'menu_position'				=> 5,
				'menu_icon'					=> 'dashicons-tag',
				'rewrite'					=> array(
							'slug' 			=> 'product'
				),
				'capabilities'				=> array(
					'edit_post'          => 'edit_product',
					'read_post'          => 'read_product',
					'delete_post'        => 'delete_product',
					'edit_posts'         => 'edit_products',
					'edit_others_posts'  => 'edit_others_products',
					'publish_posts'      => 'publish_products',  
					'read_private_posts' => 'read_private_products',
					'create_posts'       => 'edit_products', 
				)
			)	
		);

		register_post_type(
			'memberships',
			array(
				'label' 					=> 'Memberships',
				'public' 					=> false,
				'exclude_from_search'		=> false,
				'show_ui' 					=> true,
				// 'show_in_menu'				=> $this->plugin_slug,
				'show_in_menu'				=> true,
				'menu_position'				=> 6,
				'menu_icon'					=> 'dashicons-id',
				'rewrite'					=> array(
							'slug' 			=> 'membership'
				),
				'capabilities'				=> array(
					'edit_post'          => 'edit_membership',
					'read_post'          => 'read_membership',
					'delete_post'        => 'delete_membership',
					'edit_posts'         => 'edit_memberships',
					'edit_others_posts'  => 'edit_others_memberships',
					'publish_posts'      => 'publish_memberships',  
					'read_private_posts' => 'read_private_memberships',
					'create_posts'       => 'edit_memberships', 
				)
			)	
		);
	}

	function ga_remove_dashboard_widgets() {
		global $wp_meta_boxes;

		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
	}

	function ga_page_template( )
	{
		if ( is_singular( 'users' ) ) 
		{
			add_filter( 'template_include', function() 
			{
				return dirname( __FILE__ ) . '/../templates/page-user.php';
			});
		}
	}

	function create_user_role()
	{
		$roles = array(
			array(
				'role_slug' => 'ga_manager',
				'role_name' => __('Gym Manager'),
				'caps' 		=> array(
					'manage_gyms', 
					'manage_users', 
					'manage_members', 
					'manage_memberships', 
					'manage_reports',
					'remove_users',
					'list_users',
					'promote_users',
					'edit_users',
					'create_users',
					'delete_users',
					'read', 
					// User capabilities
					'edit_user', 'read_user', 'delete_user', 'edit_users', 'edit_others_users', 'publish_users', 'read_private_users', 'edit_users',
					// Products capabilities
					'edit_product', 'read_product', 'delete_product', 'edit_products', 'edit_others_products', 'publish_products', 'read_private_products', 'edit_products',
					// Memberships capabilities
					'edit_membership', 'read_membership', 'delete_membership', 'edit_memberships', 'edit_others_memberships', 'publish_memberships', 'read_private_memberships', 'edit_memberships',
					// Gyms capabilities
					'edit_gym', 'read_gym', 'delete_gym', 'edit_gyms', 'edit_others_gyms', 'publish_gyms', 'read_private_gyms', 'edit_gyms',
					// Class
					'edit_class', 'read_class', 'delete_class', 'edit_classes', 'edit_others_classes', 'publish_classes', 'read_private_classes', 'edit_classes',
				)
			),
			array(
				'role_slug' => 'ga_receptionist',
				'role_name' => __('Gym Receptionist'),
				'caps' 		=> array( 'manage_reports', 'manage_members', 'read',
					// Products capabilities
					'edit_product', 'read_product', 'edit_products', 'publish_products', 'edit_products',
					// User capabilities
					'edit_user', 'read_user', 'delete_user', 'edit_users', 'publish_users', 'edit_users',
					// Class
					'edit_class', 'read_class', 'delete_class', 'edit_classes', 'edit_others_classes', 'publish_classes', 'read_private_classes', 'edit_classes',
				)
			),
			array(
				'role_slug' => 'ga_trainer',
				'role_name' => __('Gym Trainer'),
				'caps' 		=> array( 'view_member_reports', 'view_members', 'read',
					// User capabilities
					'read_user', 'edit_users', 'edit_users',
					// Class
					'edit_class', 'read_class', 'delete_class', 'edit_classes', 'edit_others_classes', 'publish_classes', 'read_private_classes', 'edit_classes',
				)
			)
		);

		foreach ( $roles as $role )
		{
			add_role( $role['role_slug'], $role['role_name'] );
			$new_role = get_role( $role['role_slug'] );

			$current_user = new WP_User( get_current_user_id() );

			foreach ( $role['caps'] as $cap )
			{
				$new_role->add_cap( $cap );

				//Append capabilities to the administrator too
				$current_user->add_cap( $cap );
			}
		}
	}

	function remove_user_role()
	{
		$roles = array(
			array(
				'role_slug' => 'ga_manager'
			),
			array(
				'role_slug' => 'truck_driver'
			)
		);

		foreach ( $roles as $role )
		{
			remove_role( $role['role_slug'] );
		}
	}

	function ga_log_member_update($post_id, $post, $membership )
	{
		global $wpdb;


	}

	function debug( $what )
	{
		echo '<pre>'; var_dump( $what ); echo '</pre>';
	}
}