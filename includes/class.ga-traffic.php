<?php
class GA_Traffic extends GA
{
	function ga_get_traffic( $gym_id )
	{
		global $wpdb;

		$sql = "SELECT ".$wpdb->base_prefix."ga_traffic.member_id FROM ".$wpdb->base_prefix."ga_traffic WHERE ".$wpdb->base_prefix."ga_traffic.gym_id = ".$gym_id." AND DATE(".$wpdb->base_prefix."ga_traffic.enter) = DATE(NOW()) AND ".$wpdb->base_prefix."ga_traffic.leave IS NULL;";

		$results = $wpdb->get_results($sql);

		return $results;

	}

	function ga_get_traffic_today( $gym_id )
	{
		global $wpdb;

		$sql = "SELECT ".$wpdb->base_prefix."ga_traffic.member_id FROM ".$wpdb->base_prefix."ga_traffic WHERE ".$wpdb->base_prefix."ga_traffic.gym_id = ".$gym_id." AND DATE(".$wpdb->base_prefix."ga_traffic.enter) = DATE(NOW());";

		$results = $wpdb->get_results($sql);

		return count($results);

	}
}