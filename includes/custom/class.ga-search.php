<?php
class GA_Search extends GA
{
	function ga_search_cpt_by_post_and_meta_fields($args) {
		// Database abstraction
		global $wpdb;

		// Default arguments
		$defaults = array(
			'post_type' => 'post',
			'post_fields_to_search' => false,
			'meta_fields_to search' => false,
			'search_term' => false
		);

		// Extract arguments for easier access
		extract( wp_parse_args( $args, $defaults ) );

		if(($post_fields_to_search or $meta_fields_to_search) and $search_term) 
		{
			$ids = array();

			// Query part 0 - SELECT statement
			$query = "SELECT DISTINCT $wpdb->posts.ID
			FROM $wpdb->posts
			LEFT JOIN $wpdb->postmeta ON ($wpdb->posts.ID = $wpdb->postmeta.post_id)
			WHERE ($wpdb->posts.post_status = 'publish'
			AND $wpdb->posts.post_type = '$post_type') AND (";

			// Query part 1 - Search post fields
			$where_parts = array();
			if(is_array($post_fields_to_search))
			{
				foreach($post_fields_to_search as $field)
				{
					$where_parts[] = "$wpdb->posts.$field LIKE '%$search_term%'";
				}
			}

			// Query part 2 - Search meta fields
			if(is_array($meta_fields_to_search))
			{
				foreach($meta_fields_to_search as $field)
				{
					$where_parts[] = "( $wpdb->postmeta.meta_key = '$field' AND $wpdb->postmeta.meta_value LIKE '%$search_term%' )";
				}
			}

			if(count($where_parts)) $query .= implode(' OR ', $where_parts);

			// Query last part - ORDER Statement
			$query .= ") ORDER BY $wpdb->posts.ID ASC";

			$results = $wpdb->get_results($query, ARRAY_A);

			// Parse the results
			foreach($results as $result)
			{
				$ids[] = $result['ID'];
			}

			return $ids;
		}

		return false;
	}
}