<?php
class GA_CPT extends GA
{
	function init()
	{
		// Users
		add_action(	'manage_users_posts_custom_column',			array( $this, 'gym_users_custom_columns') );
		add_filter(	'manage_edit-users_columns', 				array( $this, 'gym_users_columns' ) );
		add_filter( 'manage_edit-users_sortable_columns', 		array( $this, 'gym_users_sortable' ) );

		// Membership
		add_action(	'manage_memberships_posts_custom_column',	array( $this, 'gym_memberships_custom_columns') );
		add_filter(	'manage_edit-memberships_columns', 			array( $this, 'gym_memberships_columns' ) );
		add_filter( 'manage_edit-memberships_sortable_columns', array( $this, 'gym_memberships_sortable' ) );

		// Products
		add_action(	'manage_products_posts_custom_column',		array( $this, 'gym_products_custom_columns') );
		add_filter(	'manage_edit-products_columns', 			array( $this, 'gym_products_columns' ) );
		add_filter( 'manage_edit-products_sortable_columns', 	array( $this, 'gym_products_sortable' ) );




		add_filter( 'parse_query', 								array( $this, 'ga_users_gyms_filter' ) );
		add_filter( 'parse_query', 								array( $this, 'ga_users_memberships_filter' ) );
		add_action( 'restrict_manage_posts', 					array( $this, 'ga_admin_users_gym_filter' ) );
		add_action( 'restrict_manage_posts', 					array( $this, 'ga_admin_users_memberships_filter' ) );
	}

	function ga_admin_users_gym_filter()
	{
		$type = 'post';
	    if (isset($_GET['post_type'])) {
	        $type = $_GET['post_type'];
	    }
	    //only add filter to post type you want
	    if ('users' == $type)
	    {
	    	global $wpdb;

			$querystr = "
				SELECT DISTINCT $wpdb->posts.ID, $wpdb->posts.post_title 
				FROM $wpdb->posts, $wpdb->postmeta
				WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id 
				AND $wpdb->posts.post_status = 'publish' 
				AND $wpdb->posts.post_type = 'gyms'
				AND $wpdb->posts.post_date < NOW()
				ORDER BY $wpdb->posts.post_date DESC
			";   
	        $gyms = $wpdb->get_results($querystr, OBJECT);
	        
	        ?>
	        <select name="gym">
	        	<option value=""><?php _e('All gyms ', 'gymapp'); ?></option>
		        <?php
		            $current_v = isset($_GET['gym'])? $_GET['gym']:'';
		            foreach ($gyms as $gym) {
		                printf
		                    (
		                        '<option value="%s"%s>%s</option>',
		                        $gym->ID,
		                        $gym->ID == $current_v? ' selected="selected"':'',
		                        $gym->post_title
		                    );
		                }
		        ?>
	        </select>
	        <?php
	    }

	}

	function ga_admin_users_memberships_filter()
	{
		$type = 'post';
	    if (isset($_GET['post_type'])) {
	        $type = $_GET['post_type'];
	    }
	    //only add filter to post type you want
	    if ('users' == $type)
	    {
	    	global $wpdb;

			$querystr = "
				SELECT DISTINCT $wpdb->posts.ID, $wpdb->posts.post_title 
				FROM $wpdb->posts, $wpdb->postmeta
				WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id 
				AND $wpdb->posts.post_status = 'publish' 
				AND $wpdb->posts.post_type = 'memberships'
				AND $wpdb->posts.post_date < NOW()
				ORDER BY $wpdb->posts.post_date DESC
			";   
	        $memberships = $wpdb->get_results($querystr, OBJECT);
	        
	        ?>
	        <select name="membership">
	        	<option value=""><?php _e('All memberships ', 'gymapp'); ?></option>
		        <?php
		            $current_v = isset($_GET['membership'])? $_GET['membership']:'';
		            foreach ($memberships as $membership) {
		                printf
		                    (
		                        '<option value="%s"%s>%s</option>',
		                        $membership->ID,
		                        $membership->ID == $current_v? ' selected="selected"':'',
		                        $membership->post_title
		                    );
		                }
		        ?>
	        </select>
	        <?php
	    }

	}

	function ga_users_gyms_filter($query)
	{
		global $pagenow;
	    $type = 'post';
	    if (isset($_GET['post_type'])) {
	        $type = $_GET['post_type'];
	    }
	    if ( 'users' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['gym']) && $_GET['gym'] != '') {
	        $query->query_vars['meta_key'] 		= 'gym';
	        $query->query_vars['meta_value'] 	= serialize( array( $_GET['gym'] ) );
	        // $query->query_vars[] = array(
	        // 	'meta_key' 		=> 'gym',
	        // 	'meta_value' 	=> serialize( array( $_GET['gym'] ) ),
	        // 	'compare' 	=> '='
	        // );
	    }
	}

	function ga_users_memberships_filter($query)
	{
		global $pagenow;
	    $type = 'post';
	    if (isset($_GET['post_type'])) {
	        $type = $_GET['post_type'];
	    }
	    if ( 'users' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['membership']) && $_GET['membership'] != '') {
	        $query->query_vars['meta_key'] 		= 'membership';
	        $query->query_vars['meta_value']	= serialize( array( $_GET['membership'] ) );
	        // $query->query_vars[] = array(
	        // 	'meta_key' 		=> 'membership',
	        // 	'meta_value' 	=> serialize( array( $_GET['membership'] ) ),
	        // 	'compare' 	=> '='
	        // );
	    }
	    // $this->debug($query);
	}
	
	// =================
	// Custom Post Type 
	//  	Users 
	// =================
	function gym_users_columns($columns)
	{
		$columns = array(
			'cb'	 		=> 	'<input type="checkbox" />',
			'headshot'		=>	'Headshot',
			'title' 		=> 	'Name',
			'membership' 	=> 	'Membership',
			'gym'			=>	'Gym',
			'barcode' 		=> 	'Card',
			'phone' 		=> 	'Phone',
			'email' 		=> 	'Email',
			'author' 		=> 	'User',
			'date'			=>	'Date'
		);
		return $columns;
	}

	function gym_users_sortable( $columns )
	{
		$columns['membership'] 	= 'membership';
		$columns['gym'] 		= 'gym';
		$columns['author'] 		= 'author';
		return $columns;
	}

	function gym_users_custom_columns($column)
	{
		global $post;
		if($column == 'headshot')
		{
			echo wp_get_attachment_image( get_field('headshot', $post->ID), array(50,50) );
		}
		elseif($column == 'membership')
		{
			if(get_field('membership'))
			{
				$membership = get_field('membership');
				$membership = $membership[0];
				$membership = get_post($membership);
				echo $membership->post_title;
			}
			else
			{
				echo 'n/a';
			}
		}
		elseif($column == 'gym')
		{
			if(get_field('gym'))
			{
				$gym = get_field('gym');
				$gym = $gym[0];
				$gym = get_post($gym);
				echo $gym->post_title;
			}
			else
			{
				echo 'n/a';
			}
		}
		elseif($column == 'phone')
		{
			if(get_field('phone'))
			{
				echo get_field('phone');
			}
			else
			{
				echo 'n/a';
			}
		}
		elseif($column == 'email')
		{
			if(get_field('email'))
			{
				echo get_field('email');
			}
			else
			{
				echo 'n/a';
			}
		}

		elseif($column == 'barcode')
		{
			if(get_field('barcode'))
			{
				echo 'Has Card <br />' . get_field('barcode');
			}
			else
			{
				echo 'N/A';
			}
		}
	}

	// =================
	// Custom Post Type 
	//    Membership 
	// =================
	function gym_memberships_columns($columns)
	{
		$columns = array(
			'cb'	 		=> 	'<input type="checkbox" />',
			'title' 		=> 	'Label',
			'price'			=>	'Price',
			'period' 		=> 	'Period',
			'author' 		=> 	'User',
			'date'			=>	'Date'
		);
		return $columns;
	}

	function gym_memberships_sortable( $columns )
	{
		$columns['type'] 		= 'type';
		$columns['price'] 		= 'price';
		$columns['author'] 		= 'author';
		return $columns;
	}

	function gym_memberships_custom_columns($column)
	{
		global $post;
		if($column == 'headshot')
		{
			echo wp_get_attachment_image( get_field('headshot', $post->ID), array(50,50) );
		}
		elseif($column == 'price')
		{
			if(get_field('price'))
			{
				echo get_field('price');
			}
			else
			{
				echo 'n/a';
			}
		}
		elseif($column == 'period')
		{
			if(get_field('period'))
			{
				$periods = get_field('period');
				if ( is_array( $periods ) )
				{
					foreach ( $periods as $period)
						echo $period . ', ';


				}
				else
				{
					echo $periods;
				}
			}
			else
			{
				echo 'n/a';
			}
		}
	}

	// =================
	// Custom Post Type 
	//    Products 
	// =================
	function gym_products_columns($columns)
	{
		$columns = array(
			'cb'	 		=> 	'<input type="checkbox" />',
			'title' 		=> 	'Label',
			'sell_price'	=>	'Sell Price',
			'price_inquery'	=>	'Inquery Price',
			'stoc' 			=> 	'Stoc',
			'author' 		=> 	'User',
			'date'			=>	'Date'
		);
		return $columns;
	}

	function gym_products_sortable( $columns )
	{
		$columns['type'] 		= 'type';
		$columns['sell_price']	= 'sell_price';
		$columns['author'] 		= 'author';
		return $columns;
	}

	function gym_products_custom_columns($column)
	{
		global $post;
		if($column == 'sell_price')
		{
			if(get_field('sell_price'))
			{
				echo get_field('sell_price');
			}
			else
			{
				echo 'n/a';
			}
		}
		elseif($column == 'price_inquery')
		{
			if(get_field('price_inquery'))
			{
				echo get_field('price_inquery');
			}
			else
			{
				echo 'n/a';
			}
		}
		elseif($column == 'stoc')
		{
			if(get_field('stoc'))
			{
				echo get_field('stoc');
			}
			else
			{
				echo 'n/a';
			}
		}
	}
}