<?php
class GA_Activator extends GA
{
	function activate()
	{
		$ga = new GA;
		$ga->create_user_role();
		$this->create_db();
	}

	function create_db()
	{
		global $wpdb;

		add_site_option( "ga_settings", $gaSettings );
	}
}