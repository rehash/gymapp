<?php
class GA_Store extends GA
{
	function init() {}

	function get_products() {
		$gym_id = '21';

		$query = new WP_Query(array(
			'post_type' 	=> 'products',
			'meta_query' 	=> array(
				'key' 			=> 'gym_id',
				'value'			=> serialize(array($gym_id)),
				'compare'		=> '='
			)
		));

		return $query;
	}
}